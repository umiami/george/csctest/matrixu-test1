[![pipeline status](https://gitlab.com/umiami/george/csctest/matrixu-test1/badges/master/pipeline.svg)](https://gitlab.com/umiami/george/csctest/matrixu-test1/-/pipelines?ref=master)
[![compile](https://gitlab.com/umiami/george/csctest/matrixu-test1/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/umiami/george/csctest/matrixu-test1/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/umiami/george/csctest/matrixu-test1/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/umiami/george/csctest/matrixu-test1/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/umiami/george/csctest/matrixu-test1/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/umiami/george/csctest/matrixu-test1/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Matrix

> This sample coding assignment project represents 2D matrices with two different constructors and several basic operations (methods). Note: The `Matrix` class represents `Matrix` objects, therefore its methods are not static. In order to call these methods, you must call them from an object of the `Matrix` class.

```java
Matrix m1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
Matrix m2 = new Matrix(new int[][] { { 3, 4, 5 }, { 4, 5, 6 } });
Matrix m3 = m1.plus(m2);
System.out.println(m3.toString());
```

## Lab part

1. Implement both constructors.
1. Make sure both constructor tests are passing.
1. Implement the `toString` method.
1. Make sure the appropriate test is passing.

## Assignment part

1. Implement the `equal` method first.
1. Make sure the appropriate test is passing.
1. Implement all remaining methods.
1. Make sure all remaining tests are passing.

## Sources

* [How to solve coding assignments](https://docs.google.com/document/d/1UrVbwJ5e2kDnNlxiN3vVqeRovPBC4NF0TDmf4guMmSY/edit?usp=sharing)
* [Coding Assignment Evaluation](https://github.com/InternetGuru/cae)
* [TestNG](https://testng.org/doc/index.html)
* [Maven](http://maven.apache.org)
