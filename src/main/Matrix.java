public class Matrix {
  int numRows;
  int numColumns;
  int[][] data;

  /**
   * Default constructor.
   */
  public Matrix() {
    // update
  }

  /**
   * Constructor 1 for new zero matrix.
   * It creates a matrix of correct size and initialize it to 0.
   *
   * @param rowDim number of rows
   * @param colDim number of columns
   */
  public Matrix(int rowDim, int colDim) {
    // TODO: step 1
  }

  /**
   * Constructor 2 with data for new matrix (automatically determines dimensions).
   * 1) Put the numRows to be the number of 1D arrays in the 2D array.
   * 2) Specify the numColumns and set it.
   * 3) Handle special cases properly!
   * 4) Create a new matrix to hold the data.
   * 5) Copy the data over.
   * @param source array
   */
  public Matrix(int[][] source) {
    // TODO
  }

  /**
   * It returns a String that represents this matrix, as specified in the instruction for M1.
   * Anything else IS NOT acceptable.
   */
  @Override
  public String toString() {
    // TODO
    return "";
  }

  /**
   * Compares current Matrix with given obj.
   *
   * @param obj object to compare to
   * @return a boolean indicating whether the other Object represents the same Matrix as this one
   */
  @Override
  public boolean equals(Object obj) {
    // make sure the Object we're comparing to is a Matrix
    if (!(obj instanceof Matrix)) {
      return false;
    }
    // if the above was not true, we know it's safe to treat 'obj' as a Matrix
    Matrix m = (Matrix) obj;
    // TODO
    return false;
  }

  /**
   * Multiplies current Matrix with second one.
   * This method checks if the two matrices are compatible for multiplication, if not, return null.
   * If they are compatible, determine the dimensions of the resulting matrix, and fill it in with
   * the correct values for matrix multiplication.
   *
   * @param second the Matrix to be multiplied by (the right-hand side of a multiply)
   * @return a new Matrix that is the result of this Matrix multiplied by the input Matrix or null
   */
  public Matrix times(Matrix second) {
    // TODO
    return null;
  }

  /**
   * Adds second Matrix to current one.
   * This method checks if the two matrices are compatible for addition, if not, return null.
   * If they are compatible, create a new matrix and fill it in with
   * the correct values for matrix addition.
   *
   * @param second the Matrix to be added (right-hand side of addition)
   * @return a new Matrix that is the result of this Matrix added to the input Matrix or null
   */
  public Matrix plus(Matrix second) {
    // TODO
    return null;
  }

  /**
   * Transpose current Matrix.
   *
   * @return a new matrix that is the transpose of this matrix
   */
  public Matrix transpose() {
    // TODO
    return null;
  }

}
